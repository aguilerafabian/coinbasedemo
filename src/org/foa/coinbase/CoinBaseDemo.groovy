package org.foa.coinbase

import java.io.IOException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

import org.apache.commons.codec.binary.Hex
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.util.EntityUtils

import groovy.json.JsonSlurper

public class CoinBaseDemo {

	public static String getNonce() {
		String nonce = String.valueOf(System.currentTimeMillis())

		println("nonce    : " + nonce)

		return nonce
	}

	public static String getMessage(String url, String body, String nonce) {
		String message = nonce + url + (body != null ? body : "")

		println("message  : " + message)

		return message
	}

	public static String getSignature(String message, String apiSecret) {
		Mac mac = Mac.getInstance("HmacSHA256")
		mac.init(new SecretKeySpec(apiSecret.getBytes(), "HmacSHA256"))
		String signature = new String(Hex.encodeHex(mac.doFinal(message.getBytes())))

		println("signature: " + signature)

		return signature
	}

	public static String getHttp(String url, String apiKey, String apiSecret, String body)
			throws InvalidKeyException, NoSuchAlgorithmException,
			ClientProtocolException, IOException {

		String nonce     = getNonce()
		String message   = getMessage(url, body, nonce)
		String signature = getSignature(message, apiSecret)

		println("\nHeaders")
		println("-------")
		println("ACCESS_KEY      : " + apiKey)
		println("ACCESS_SIGNATURE: " + signature)
		println("ACCESS_NONCE    : " + nonce)

		HttpRequestBase request
		if (body == null || body.length() == 0) {
			println("\nGET from url: " + url)
			request = new HttpGet(url)
		} else {
			println("\nPOST to url: " + url)
			println("\nbody: " + body)

			HttpPost post = new HttpPost(url)
			post.setEntity(new StringEntity(body))
			request = post
		}

		request.setHeader("ACCESS_KEY",       apiKey)
		request.setHeader("ACCESS_SIGNATURE", signature)
		request.setHeader("ACCESS_NONCE",     nonce)

		HttpClient httpClient = HttpClientBuilder.create().build()
		HttpResponse response = httpClient.execute(request)

		HttpEntity entity = response.getEntity()
		if (entity != null) {
			String result = EntityUtils.toString(entity)
			println("\nresult: " + result)

			return result
		}
	}

	public static void showHelp() {
		println("CoinBaseDemo")
		println("------------")
		println("")
		println("coinBaseDemo <action> <apiKey> <apiSecret>")
		println("")
		println("<action>    : get_balance | get_checkout")
		println("<apiKey>    : your CoinBase api_key")
		println("<apiSecret> : your CoinBase api_secret")
	}

	public static void main(String[] args) throws Exception {
		if (args.size() != 3) {
			showHelp()
			return
		}

		String action    = args[0]
		String apiKey    = args[1]
		String apiSecret = args[2]

		println("action   : " + action)
		println("apiKey   : " + apiKey)
		println("apiSecret: " + apiSecret)
		println("")

		if (action == "get_balance") {
			String url = "https://api.coinbase.com/v1/account/balance"

			getHttp(url, apiKey, apiSecret, null)

			println("\ndone.")
			
			return
		}

		if (action == "get_checkout") {
			String url = "https://api.coinbase.com/v1/buttons"
			String body = "{ \"button\": { \"name\": \"test\", \"type\": \"buy_now\", \"subscription\": \"false\", \"price_string\": \"0.50\", \"price_currency_iso\": \"ARS\", \"custom\": \"Order123\", \"callback_url\": \"http://54.148.156.169/org/foa/callbacks/coinbase\", \"description\": \"Sample description\", \"style\": \"custom_large\", \"include_email\": \"false\" } }"

			String result = getHttp(url, apiKey, apiSecret, body)

			JsonSlurper jsonSlurper = new JsonSlurper()
			def resultJson = jsonSlurper.parseText(result)
			
			String id = resultJson.button.code
			println("\nBrowse this URL to pay: https://www.coinbase.com/checkouts/" + id)

			println("\ndone.")
			
			return
		}

		showHelp()
	}
}
