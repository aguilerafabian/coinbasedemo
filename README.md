coinbasedemo
============

CoinBase API demo, let's you:

	1.Get the balance of an acount
	2.Generate a payment button example

https://www.coinbase.com allows you to sell/buy bitcoins, also provides a VERY customizable environment.


How to run (from shell):

	$ coinBaseDemo.sh <action> <apiKey> <apiSecret>

How to run (inside GGTS):

	Add to the classpath every JAR file in /lib

	Example to run from GGTS:
	--classpath "${workspace_loc:/coinbasedemo}:./lib/commons-codec-1.6.jar:./lib/commons-logging-1.1.3.jar:./lib/fluent-hc-4.3.5.jar:./lib/httpclient-4.3.5.jar:./lib/httpclient-cache-4.3.5.jar:./lib/httpcore-4.3.2.jar:./lib/httpmime-4.3.5.jar:${workspace_loc:/coinbasedemo}" --main groovy.ui.GroovyMain "${resource_loc:/coinbasedemo/org/foa/coinbase/CoinBaseDemo.groovy}" get_checkout <apiKey> <apiSecret>

How it works:

	coinBaseDemo <action> <apiKey> <apiSecret>

	<action>    : get_balance | get_checkout
	<apiKey>    : your CoinBase api_key
	<apiSecret> : your CoinBase api_secret
